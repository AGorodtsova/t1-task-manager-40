package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectClearRequest;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.gorodtsova.tm.dto.request.task.*;
import ru.t1.gorodtsova.tm.dto.request.user.UserLoginRequest;
import ru.t1.gorodtsova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.gorodtsova.tm.dto.response.task.*;
import ru.t1.gorodtsova.tm.enumerated.TaskSort;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.Task;

import static ru.t1.gorodtsova.tm.enumerated.Status.COMPLETED;
import static ru.t1.gorodtsova.tm.enumerated.Status.IN_PROGRESS;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    private String userToken;

    private Project project;

    private Task task;

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest("test", "test");
        userToken = authEndpoint.login(request).getToken();

        @Nullable final ProjectCreateRequest projectCreateRequest =
                new ProjectCreateRequest(userToken, "name", "description");
        project = projectEndpoint.createProject(projectCreateRequest).getProject();

        @Nullable final TaskCreateRequest taskCreateRequest =
                new TaskCreateRequest(userToken, "name", "description");
        task = taskEndpoint.createTask(taskCreateRequest).getTask();
    }

    @After
    public void tearDown() {
        @Nullable final TaskClearRequest requestClearTask = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(requestClearTask);

        @Nullable final ProjectClearRequest requestClearProject = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClearProject);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test
    public void createTask() {
        @Nullable final TaskCreateRequest request =
                new TaskCreateRequest(userToken, "new name", "new description");
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        Task task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new description", task.getDescription());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest(userToken, null, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest("wrongToken", "new name", "new description")
                )
        );
    }

    @Test
    public void listTask() {
        @Nullable final TaskListRequest request =
                new TaskListRequest(userToken, TaskSort.BY_DEFAULT);
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        Assert.assertEquals(response.getTasks().size(), 1);
        Assert.assertNotNull(response.getTasks().get(0));
        Assert.assertEquals(task.getId(), response.getTasks().get(0).getId());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest(userToken, null))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest("", TaskSort.BY_DEFAULT))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest("wrongToken", TaskSort.BY_DEFAULT))
        );
    }

    @Test
    public void listTaskByProjectId() {
        @Nullable final TaskBindToProjectRequest taskBindToProjectRequest =
                new TaskBindToProjectRequest(userToken, project.getId(), task.getId());
        @Nullable TaskBindToProjectResponse taskBindToProjectResponse =
                taskEndpoint.bindTaskToProject(taskBindToProjectRequest);
        Assert.assertNotNull(taskBindToProjectResponse);

        @Nullable final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(userToken, project.getId());
        @Nullable final TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTasks());
        Assert.assertEquals(1, response.getTasks().size());
        Assert.assertEquals(task.getId(), response.getTasks().get(0).getId());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.listTaskByProjectId(new TaskListByProjectIdRequest("wrongToken", project.getId()))
        );
    }

    @Test
    public void showTaskById() {
        @Nullable final TaskShowByIdRequest taskShowByIdRequest =
                new TaskShowByIdRequest(userToken, task.getId());
        @Nullable final TaskShowByIdResponse taskShowByIdResponse =
                taskEndpoint.showTaskById(taskShowByIdRequest);
        Assert.assertNotNull(taskShowByIdResponse);
        Assert.assertEquals(task.getId(), taskShowByIdResponse.getTask().getId());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest("wrongToken", task.getId()))
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.showTaskById(new TaskShowByIdRequest(userToken, null))
        );
    }

    @Test
    public void updateTaskById() {
        @NotNull final String taskId = task.getId();

        @Nullable final TaskUpdateByIdRequest taskUpdateByIdRequest =
                new TaskUpdateByIdRequest(userToken, taskId, "new name", "new description");
        @Nullable final Task task =
                taskEndpoint.updateTaskById(taskUpdateByIdRequest).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new description", task.getDescription());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest("wrongToken", taskId, "name", "description")
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.updateTaskById(
                        new TaskUpdateByIdRequest(userToken, null, "name", "description")
                )
        );
    }

    @Test
    public void clearTask() {
        @Nullable final TaskListRequest taskListRequest =
                new TaskListRequest(userToken, TaskSort.BY_DEFAULT);
        Assert.assertFalse(taskEndpoint.listTask(taskListRequest).getTasks().isEmpty());
        taskEndpoint.clearTask(new TaskClearRequest(userToken));
        Assert.assertNull(
                taskEndpoint.listTask(new TaskListRequest(userToken, TaskSort.BY_DEFAULT)).getTasks()
        );

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.clearTask(new TaskClearRequest("wrongToken"))
        );
    }

    @Test
    public void removeTaskById() {
        @Nullable final TaskListRequest taskListRequest =
                new TaskListRequest(userToken, TaskSort.BY_DEFAULT);
        Assert.assertFalse(taskEndpoint.listTask(taskListRequest).getTasks().isEmpty());
        @Nullable final TaskRemoveByIdRequest request =
                new TaskRemoveByIdRequest(userToken, task.getId());
        taskEndpoint.removeTaskById(request);
        Assert.assertNull(
                taskEndpoint.listTask(new TaskListRequest(userToken, TaskSort.BY_DEFAULT)).getTasks()
        );

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest("wrongToken", task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.removeTaskById(
                        new TaskRemoveByIdRequest(userToken, null)
                )
        );
    }

    @Test
    public void changeStatusById() {
        @NotNull final String taskId = task.getId();
        @Nullable final TaskChangeStatusByIdRequest request =
                new TaskChangeStatusByIdRequest(userToken, taskId, IN_PROGRESS);
        @Nullable final Task task = taskEndpoint.changeStatusById(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(IN_PROGRESS, task.getStatus());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeStatusById(new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeStatusById(
                        new TaskChangeStatusByIdRequest("wrongToken", taskId, IN_PROGRESS)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.changeStatusById(
                        new TaskChangeStatusByIdRequest("wrongToken", null, IN_PROGRESS)
                )
        );
    }

    @Test
    public void startTaskById() {
        Assert.assertNotEquals(IN_PROGRESS.toString(), task.getStatus().toString());
        @Nullable final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken, task.getId());
        @Nullable final Task newTask = taskEndpoint.startTaskById(request).getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(IN_PROGRESS.toString(), newTask.getStatus().toString());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(
                        new TaskStartByIdRequest("wrongToken", task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.startTaskById(
                        new TaskStartByIdRequest(userToken, null)
                )
        );
    }

    @Test
    public void completeTaskById() {
        Assert.assertNotEquals(COMPLETED.toString(), task.getStatus().toString());
        @Nullable final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken, task.getId());
        @Nullable final Task newTask = taskEndpoint.completeTaskById(request).getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(COMPLETED.toString(), newTask.getStatus().toString());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskById(new TaskCompleteByIdRequest())
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskById(
                        new TaskCompleteByIdRequest("wrongToken", task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.completeTaskById(
                        new TaskCompleteByIdRequest(userToken, null)
                )
        );
    }

    @Test
    public void bindTaskToProject() {
        @Nullable TaskBindToProjectResponse taskBindToProjectResponse = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(userToken, project.getId(), task.getId())
        );
        Assert.assertNotNull(taskBindToProjectResponse);

        @Nullable final TaskShowByIdResponse taskShowByIdResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, task.getId())
        );
        Assert.assertNotNull(taskShowByIdResponse);
        Assert.assertNotNull(taskShowByIdResponse.getTask());
        Assert.assertEquals(project.getId(), taskShowByIdResponse.getTask().getProjectId());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest("wrongToken", project.getId(), task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(userToken, null, task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(userToken, project.getId(), null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.bindTaskToProject(
                        new TaskBindToProjectRequest(userToken, "0-1", task.getId())
                )
        );
    }

    @Test
    public void unbindTaskToProject() {
        @Nullable TaskBindToProjectResponse taskBindToProjectResponse = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(userToken, project.getId(), task.getId())
        );
        Assert.assertNotNull(taskBindToProjectResponse);

        @Nullable TaskUnbindFromProjectResponse taskUnbindFromProjectResponse = taskEndpoint.unbindTaskToProject(
                new TaskUnbindFromProjectRequest(userToken, project.getId(), task.getId())
        );
        Assert.assertNotNull(taskUnbindFromProjectResponse);
        @Nullable final TaskShowByIdResponse taskShowByIdResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, task.getId())
        );
        Assert.assertNotNull(taskShowByIdResponse);
        Assert.assertNotNull(taskShowByIdResponse.getTask());
        Assert.assertNotEquals(project.getId(), taskShowByIdResponse.getTask().getProjectId());
        Assert.assertNull(taskShowByIdResponse.getTask().getProjectId());

        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.unbindTaskToProject(
                        new TaskUnbindFromProjectRequest("wrongToken", project.getId(), task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.unbindTaskToProject(
                        new TaskUnbindFromProjectRequest(userToken, null, task.getId())
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.unbindTaskToProject(
                        new TaskUnbindFromProjectRequest(userToken, project.getId(), null)
                )
        );
        Assert.assertThrows(
                Exception.class,
                () -> taskEndpoint.unbindTaskToProject(
                        new TaskUnbindFromProjectRequest(userToken, "0-1", task.getId())
                )
        );
    }

}
