package ru.t1.gorodtsova.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRequest extends AbstractRequest {

    @NotNull
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@NotNull String login, @Nullable String password) {
        this.login = login;
        this.password = password;
    }

}
