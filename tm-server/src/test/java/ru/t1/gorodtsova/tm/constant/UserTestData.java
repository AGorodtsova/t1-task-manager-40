package ru.t1.gorodtsova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@UtilityClass
public class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User ADMIN1 = new User();

    @NotNull
    public final static List<User> USER_LIST1 = Arrays.asList(USER1, USER2);

    @NotNull
    public final static List<User> USER_LIST2 = Collections.singletonList(ADMIN1);

    @NotNull
    public final static List<User> USER_LIST = new ArrayList<>();

    static {
        USER_LIST.addAll(USER_LIST1);
        USER_LIST.addAll(USER_LIST2);

        for (int i = 0; i < USER_LIST.size(); i++) {
            @NotNull final User user = USER_LIST.get(i);
            user.setId("t-0" + i);
            user.setLogin("login" + i);
            user.setEmail("user" + i + "@company.ru");
        }
    }

}
