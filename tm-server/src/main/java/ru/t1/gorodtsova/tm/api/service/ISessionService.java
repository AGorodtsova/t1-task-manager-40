package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Session;

public interface ISessionService {

    @NotNull
    Session create(@NotNull Session session);

    @Nullable
    Session findOneById(@Nullable String id);

    @NotNull
    Session removeOneById(@Nullable String id);

    boolean existsById(@Nullable String id);

}
