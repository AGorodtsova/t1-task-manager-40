package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.enumerated.TaskSort;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@Nullable Task task);

    @NotNull
    Task add(@Nullable String userId, @Nullable Task task);

    @NotNull
    Collection<Task> add(@Nullable Collection<Task> tasks);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Collection<Task> set(@Nullable Collection<Task> tasks);

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@NotNull String userId);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    void removeAll();

    void removeAll(@Nullable String userId);

    @NotNull
    Task removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    int getSize();

    int getSize(@Nullable final String userId);

}
